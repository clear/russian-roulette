package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func paste(content []byte, title string) string {
	web := "https://pastebin.com"
	res, err := http.Get(web)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Fatalf("Failed to fetch data: %d\n", res.StatusCode)
	}
	tokenstring := res.Header.Get("Set-Cookie")
	tokenstrings := strings.Split(tokenstring, ";")
	cookie := tokenstrings[0]
	if cookie == "" {
		log.Fatalln("No cookie")
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	csrftoken, exists := doc.Find("meta[name='csrf-token']").Attr("content")
	if !exists {
		log.Fatalln("No token tag found")
	}
	vals := url.Values{}
	vals.Add("_csrf-frontend", csrftoken)
	vals.Add("PostForm[text]", string(content))
	vals.Add("PostForm[format]", "1")
	vals.Add("PostForm[expiration]", "N")
	vals.Add("PostForm[status]", "0")
	vals.Add("PostForm[is_password_enabled]", "0")
	vals.Add("PostForm[is_burn]", "0")
	vals.Add("PostForm[name]", title)
	req, err := http.NewRequest("POST", web, strings.NewReader(vals.Encode()))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("cookie", cookie)

	c := &http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(b))
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	link, exists := doc.Find("meta[property='og:url']").Attr("content")

	if !exists {
		b, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(b))
		log.Fatalln("No link found")
	}
	return link
}
