package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

var bulletstyle = lipgloss.NewStyle().Background(lipgloss.Color("9"))

type model struct {
	chambers []bool
	bullet   int
	firing   bool
}

func (m model) Init() tea.Cmd {
	return initial
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	if m.firing {
		return m, tea.Quit
	}
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		}
	case spinMsg:
		if int(msg) == 0 {
			m.firing = true
			m.chambers = []bool{false, false, false, false, false, false}
			m.chambers[0] = true //m.bullet] = true
			return m, nil
		} else {
			if m.bullet < 5 {
				m.bullet++
			} else {
				m.bullet = 0
			}
			m.chambers = []bool{false, false, false, false, false, false}
			m.chambers[m.bullet] = true
			return m, spin(int(msg))
		}
	}

	return m, nil
}

func (m model) View() string {
	if m.firing {
		var c [6]string
		for pos, chamber := range m.chambers {
			if chamber {
				c[pos] = bulletstyle.Render("0")
			} else {
				c[pos] = "0"
			}
		}
		if m.chambers[0] {
			message := bulletstyle.Render("BANG! YOU LOSE")
			link := paste([]byte("test\ntest\ntest\nid_rsa"), "teststirng")
			return fmt.Sprintf("%s\n     %s     \n  %s     %s    \n  %s     %s   \n     %s      \n%s", message, c[0], c[5], c[1], c[4], c[2], c[3], link)

		} else {
			message := "You win...this time"
			return fmt.Sprintf("%s\n     %s     \n  %s     %s    \n  %s     %s   \n     %s      ", message, c[0], c[5], c[1], c[4], c[2], c[3])
		}
	} else {
		var c [6]string
		for pos, chamber := range m.chambers {
			if chamber {
				c[pos] = bulletstyle.Render("0")
			} else {
				c[pos] = "0"
			}
		}
		return fmt.Sprintf("     %s     \n  %s     %s    \n  %s     %s   \n     %s      ", c[0], c[5], c[1], c[4], c[2], c[3])
	}
}

type spinMsg int

func initial() tea.Msg {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	rounds := r1.Intn(25) + 1
	return spinMsg(rounds)
}

func spin(c int) tea.Cmd {
	time.Sleep(300 * time.Millisecond)
	return func() tea.Msg {
		return spinMsg(c - 1)
	}
}

func main() {
	p := tea.NewProgram(model{chambers: []bool{false, false, false, false, false, false}, bullet: 0})
	if _, err := p.Run(); err != nil {
		fmt.Printf("Error starting program: %v", err)
		os.Exit(1)
	}
}
